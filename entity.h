#ifndef ENTITY_H
#define ENTITY_H

#include <SDL2/SDL.h>
#include <vector>
#include "window.h"
using namespace std;

/**
 * Base entity class
 */
class Entity
{
public:
    virtual ~Entity() {};
    virtual void HandleEvents(const SDL_Event &event);
    virtual void Update(vector<Entity*> &entities);
    virtual void Draw(SDL_Rect &camera, Window &window);
    virtual Rect *get_rect() = 0;
};

#endif
