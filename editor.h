#ifndef EDITOR_H
#define EDITOR_H

#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include "rect.h"

using namespace std;

class Editor
{
public:
    Editor();
    ~Editor();
    //allow editing
    void GameLoop();
    //save the level to filename
    void Save(string filename, vector<pair<Rect*, int> > entities);
private:
    //have we quit?
    bool quit;
    SDL_Event e;
};
#endif
