#include <cassert>
#include "rect.h"
#include "player.h"
#include "enemy.h"
#include "window.h"
using namespace std;

void TestRectMove()
{
    Window window;
    Rect rect("mario.png", window);
    Rect rect2("mario.png", window);
    rect.Move(5, 5);
    assert(rect.get_x() == 5);
    assert(rect.get_y() == 5);
    rect2.Move(400, 400);
    assert(rect2.get_x() == 400);
    assert(rect2.get_y() == 400);
}

void TestRectNotColliding()
{
    Window window;
    Rect rect("mario.png", window);
    Rect rect2("mario.png", window);
    rect.Move(5, 5);
    assert(rect.get_x() == 5);
    assert(rect.get_y() == 5);
    rect2.Move(400, 400);
    assert(rect2.get_x() == 400);
    assert(rect2.get_y() == 400);
    assert(!rect.CheckCollision(rect2));

}
void TestColliding()
{
    Window window;
    Rect rect("mario.png", window);
    Rect rect2("mario.png", window);
    rect.Move(5, 5);
    assert(rect.get_x() == 5);
    assert(rect.get_y() == 5);
    rect2.Move(6, 6);
    assert(rect2.get_x() == 6);
    assert(rect2.get_y() == 6);
    assert(rect.CheckCollision(rect2));
}
void TestPlayerEnemy()
{
    Window window;
    Player player(window);
    Enemy enemy(window);
    assert(!player.HandleEnemy(&enemy));
}

int main()
{
   TestRectMove(); 
   TestRectNotColliding(); 
   TestColliding();
   TestPlayerEnemy();
}
