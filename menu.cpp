#include "menu.h"
#include <vector>
#include <string>
#include <iostream>

using namespace std;

Menu::Menu(vector<string> level_names)
{
    quit = false;
    this->level_names = level_names;
    cur_level = 0;
}

Menu::~Menu()
{
}

int Menu::Loop()
{
    vector<Rect *> menu_items;
    Window window(800, 600);
    SDL_Rect camera = {0, 0, 800, 600};
    SDL_Color black = {0, 0, 0};
    //populate menu items
    for(int i = 0; i < level_names.size(); i++)
    {
        menu_items.push_back(new Rect("times.ttf", level_names[i], black));
        menu_items[i]->Move(400 - menu_items[i]->get_w()/2, i * 30 + 10);
    }
    while(!quit)
    {
        delete menu_items[cur_level];
        //color the currently selected item
        SDL_Color text_color = {238, 130, 238};
        menu_items[cur_level] = new Rect("times.ttf", level_names[cur_level], text_color);
        menu_items[cur_level]->Move(400 - menu_items[cur_level]->get_w()/2, cur_level * 30 + 10);
        //handle input
        while(SDL_PollEvent(&e) != 0)
        {
            if(e.type == SDL_QUIT)
                quit = true;
            if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
            {
                switch (e.key.keysym.sym)
                {
                //move around the menu
                case SDLK_DOWN:
                    delete menu_items[cur_level];
                    menu_items[cur_level] = new Rect("times.ttf", level_names[cur_level], black);
                    menu_items[cur_level]->Move(400 - menu_items[cur_level]->get_w()/2, cur_level * 30 + 10);
                    cur_level = (cur_level + 1) % level_names.size();
                    break;
                case SDLK_UP:
                    delete menu_items[cur_level];
                    menu_items[cur_level] = new Rect("times.ttf", level_names[cur_level], black);
                    menu_items[cur_level]->Move(400 - menu_items[cur_level]->get_w()/2, cur_level * 30 + 10);
                    cur_level--;
                    while(cur_level < 0)
                        cur_level += level_names.size();
                    break;
                //select a menu item
                case SDLK_RETURN:
                    return cur_level;
                    break;
                }
            }
        }
        window.ClearWindow();
        //draw the menu items
        for(int i = 0; i < menu_items.size(); i++)
        {
            menu_items[i]->Draw(camera, window, 0, 0);
        }
        window.Update();
        SDL_Delay(100);
    }
    return -1;
}
