#ifndef PLAYER_H
#define PLAYER_H

#include "entity.h"
#include "enemy.h"
#include <SDL2/SDL.h>

/**
 * Main player class, inherits entity
 */
class Player: public Entity
{
public:
    Player(Window &window);
    ~Player();
    //handle events
    void HandleEvents(const SDL_Event &event);
    void Update(vector<Entity *> entities);
    int HandleEnemy(Enemy *enemy);
    void Draw(SDL_Rect &camera, Window &window);
    Rect *get_rect();
private:
    //rect when facing right, flipped rect when facing left
    Rect *rect;
    Rect *flipped_rect;
    Rect *text;
    bool flipped;
    //x and y velocity
    int vx, vy;
    //did player jump?
    bool jump;
};

#endif
