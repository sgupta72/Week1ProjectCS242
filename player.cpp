#include "player.h"
#include <SDL2/SDL.h>
#include <vector>
#include <iostream>

using namespace std;

Player::Player(Window &window)
{
    //set up right facing rect and animations
    rect = new Rect("mario.png", window, 1, 4);
    vector<pair<int, int> > mario_animations;
    for(int i = 0; i < 1; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            mario_animations.push_back(make_pair(i, j));
        }
    }
    rect->SetAnimationFrames(mario_animations);
    //set up left facing rect and animations
    flipped_rect = new Rect("mario_flipped.png", window, 1, 4);
    vector<pair<int, int> > mario_flipped_animations;
    for(int i = 0; i < 1; i++)
    {
        for(int j = 3; j >= 0; j--)
        {
            mario_flipped_animations.push_back(make_pair(i, j));
        }
    }
    flipped_rect->SetAnimationFrames(mario_flipped_animations);
    SDL_Color text_color = { 0, 0, 0 };
    text = new Rect("times.ttf", "something Yes", text_color);
    //not jumping
    jump = false;
    //facing right
    flipped = false;
    //no velocity
    vx = 0;
    vy = 0;
}

Player::~Player()
{
    delete rect;
    delete flipped_rect;
}

void Player::HandleEvents(const SDL_Event &e)
{
    if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
    {
        switch (e.key.keysym.sym)
        {
            //pressed up -> jump
            case SDLK_UP:
                jump = true;
                break;
            //move to left
            case SDLK_LEFT:
                vx -= 20;
                flipped = true;
                break;
            //move to right 
            case SDLK_RIGHT:
                vx += 20;
                flipped = false;
                break;
        }
    }
    else if(e.type == SDL_KEYUP && e.key.repeat == 0)
    {
        switch (e.key.keysym.sym)
        {
            //stop moving left
            case SDLK_LEFT:
                vx += 20;
                break;
            //stop moving right 
            case SDLK_RIGHT:
                vx -= 20;
                break;
        }
    }
}

void Player::Update(vector<Entity *> entities)
{
    //gravity
    vy += 15;
    //perform jump
    if(jump)
    {
        vy = -50;
        jump = false;
    }
    //move rects
    rect->Move(rect->get_x() + vx, rect->get_y() + vy);
    flipped_rect->Move(rect->get_x() + vx, rect->get_y() + vy);
    //handle collisions
    for(int i = 0; i < entities.size(); i++)
    {
        if(rect->CheckCollision(*(entities[i]->get_rect())))
            vy = 0;
        rect->CorrectPosition(entities[i]->get_rect());
        flipped_rect->CorrectPosition(entities[i]->get_rect());
    }
}

int Player::HandleEnemy(Enemy *enemy)
{
    //killed enemy!
    if(vy > 0 && rect->CheckCollision(*(enemy->get_rect())))
        return 1;
    //died!
    if(vy <= 0 && rect->CheckCollision(*enemy->get_rect()))
        return 2;
    //nothing happened
    return 0;
}
void Player::Draw(SDL_Rect &camera, Window &window)
{
    if(!flipped)
    {
        if(vx == 0)
            rect->Draw(camera, window, 0, 0);
        else
            rect->Draw(camera, window);
    }
    else
    {
        if(vx == 0)
            flipped_rect->Draw(camera, window, 0, 3);
        else
            flipped_rect->Draw(camera, window);
    }
    camera.x = rect->get_x() - 400;
    camera.y = rect->get_y() - 300;
}

Rect *Player::get_rect()
{
    return rect;
}
