#include "platform.h"

Platform::Platform(Window &window, int x, int y)
{
    //create platform at (x, y)
    rect = new Rect("platform.png", window);
    rect->Move(x, y);
}

Platform::~Platform()
{
    delete rect;
}

void Platform::Draw(SDL_Rect &camera, Window &window)
{
    rect->Draw(camera, window);
}
Rect *Platform::get_rect()
{
    //get rectangle of platform
    return rect;
}
