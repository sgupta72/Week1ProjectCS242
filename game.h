#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <string>

using namespace std;
/**
 * Main game class that contains game loop
**/
class Game
{
public:
    Game(string filename);
    ~Game();
    //main game loop
    void GameLoop(string filename);
private:
    //has the user quit?
    bool quit;
    bool died;
    bool won;
    int collected;
    //current event
    SDL_Event e;
    SDL_Rect camera;
};
#endif
