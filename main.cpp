#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include "editor.h"
#include "game.h"
#include "menu.h"
using namespace std;

int main()
{
    /*if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        cout << "Could not initialize SDL: " << SDL_GetError() << endl;
        return 0;
    }
    cout << IMG_INIT_PNG << endl;
    if(!IMG_Init(IMG_INIT_PNG))
    {
        cout << "Could not initialize SDL_image: " << IMG_GetError() << endl;
        return 0;
    }
    if(TTF_Init() == -1)
    {
        cout << "Could not initialize SDL_TTF: " << TTF_GetError() << endl;
        return 0;
    }
    Editor game;*/
    //init level names
    vector<string> level_names;
    level_names.push_back("Level 1");
    level_names.push_back("Level 2");
    level_names.push_back("Level 3");
    //init level files
    vector<string> level_files;
    level_files.push_back("level1.txt");
    level_files.push_back("level2.txt");
    level_files.push_back("level3.txt");
    int level = 1;
    while(level != -1)
    {
        if(SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            cout << "Could not initialize SDL: " << SDL_GetError() << endl;
            return 0;
        }
        cout << IMG_INIT_PNG << endl;
        if(!IMG_Init(IMG_INIT_PNG))
        {
            cout << "Could not initialize SDL_image: " << IMG_GetError() << endl;
            return 0;
        }
        if(TTF_Init() == -1)
        {
            cout << "Could not initialize SDL_TTF: " << TTF_GetError() << endl;
            return 0;
        }
        Menu *menu = new Menu(level_names);
        SDL_Delay(100);
        level = menu->Loop();
        delete menu;
        if(level != -1)
        {
            SDL_Delay(100);
            Game *game = new Game(level_files[level]);
            delete game;
        }
    }
}
