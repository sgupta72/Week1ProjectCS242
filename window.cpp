#include "window.h"
#include <SDL2/SDL.h>
#include <iostream>
#include "rect.h"

using namespace std;

Window::Window(int width, int height)
{
    //create window
    window = SDL_CreateWindow("Platformer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
    if(window == NULL)
    {
        cout << "Window could not be created: " << SDL_GetError() << endl;
        return;
    }
    cout << "HERE" << endl;
    screen_surface = SDL_GetWindowSurface(window);
    //fill it with white
    SDL_FillRect(screen_surface, NULL, SDL_MapRGB(screen_surface->format, 0xFF, 0xFF, 0xFF));
    SDL_UpdateWindowSurface(window);
}

Window::~Window()
{
    //destructor of window
    SDL_DestroyWindow(window);
    window = NULL;
}

void Window::ClearWindow()
{
    //clear the window with white
    SDL_FillRect(screen_surface, NULL, SDL_MapRGB(screen_surface->format, 0xFF, 0xFF, 0xFF));
}

SDL_Surface *Window::get_screen_surface()
{
    return screen_surface;
}

void Window::Update()
{
    //update window
    SDL_UpdateWindowSurface(window);
}
