#ifndef RECT_H
#define RECT_H

#include <string>
#include <SDL2/SDL.h>
#include <vector>
#include "window.h"
#include <SDL2/SDL_ttf.h>

using namespace std;

/**
 * Class to render rectangles and handle collision detection between rectangles
**/
class Window;
class Rect
{
public:
    /**
     * load spritesheet
     * filename: filaname of image file
     * window: window to draw on
     */
    Rect(string filename, Window &window);
    /**
     * load spritesheet
     * filename: filaname of image file
     * window: window to draw on
     * rows: rows in spritesheet
     * column: columns in spritesheet
     */
    Rect(string filename, Window &window, int rows, int columns);

    /**
     * load text
     * font_filename: filaname of font file
     * text: string to render
     * text_color: color to render in
     */
    Rect(string font_filename, string text, SDL_Color text_color);
    ~Rect();
    /**
     * move rect to x, y coordinate
     */
    void Move(int x, int y);
    /**
     * add velocity to movement
    **/
    //void AddVelocity(int vx, int vy);
    /**
     * update position of rect
    **/
    //void UpdatePosition();
    /**
     * draw the image
     * window: window to draw on
    **/
    void Draw(const SDL_Rect &camera, Window &window);
    /**
     * render part of spritesheet on window 
     * window: window to draw on
     * row: row of spritesheet
     * column: column of spritesheet
    **/
    void Draw(const SDL_Rect &camera, Window &window, int row, int column);
    /**
     * check collision with rect
     * rect: rectangle ot check against
    **/
    bool CheckCollision(Rect &rect);
    /**
     * move back if colliding with rect in rects
     * rect: rect to check against
    **/
    void CorrectPosition(Rect *rect);
    /**
     * move back if colliding with rect in rects
     * rects: rect vector to check against
    **/
    void CorrectPosition(vector<Rect*> rects);
    /**
     * get image surface
    **/
    SDL_Surface *get_image();

    int get_x();

    int get_y();

    int get_w();

    int get_h();
        
    SDL_Rect get_rect();
    //set frames to use for animation
    void SetAnimationFrames(vector<pair<int, int> > animation_frames);
private:
    /**
     * actually load image
     * filename: file to load from
     * window: window to draw on
     */
    void LoadImage(string filename, Window &window);
    SDL_Surface *image;
    SDL_Rect sdl_rect;
    int rows, columns;
    vector<pair<int, int> > animation_frames;
    int current_frame;
};
#endif
