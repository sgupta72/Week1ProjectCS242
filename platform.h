#ifndef PLATFORM_H
#define PLATFORM_H

#include "entity.h"
#include "window.h"
#include "rect.h"

class Platform: public Entity
{
public:  
    Platform(Window &window, int x, int y);
    ~Platform();
    void Draw(SDL_Rect &camera, Window &window);
    Rect *get_rect();  
private:
    Rect *rect;
};
#endif
