#ifndef ENEMY_H
#define ENEMY_H

#include "entity.h"
/**
 * Main enemy class, inherits entity class
 */
class Enemy: public Entity
{
public:
    Enemy(Window &window, int x, int y);
    ~Enemy();
    void Update(vector<Entity *> entities);
    //draw enemy using camera
    void Draw(SDL_Rect &camera, Window &window);
    Rect *get_rect();
private:
    //store rect and the flipped version flipped rect
    Rect *rect;
    Rect *flipped_rect;
    //store y velocity of enemy
    int vy;
    //is enemy going left or right?
    bool right;
    int x, y;
};
#endif
