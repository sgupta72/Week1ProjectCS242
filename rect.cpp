#include "rect.h"
#include "window.h"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>

using namespace std;

void Rect::LoadImage(string filename, Window &window)
{
    //load image into surface
    SDL_Surface *loaded = IMG_Load(filename.c_str());
    if(loaded == NULL)
    {
        cout << "Unable to load image: " << filename  << " " << IMG_GetError() << endl;
        return;
    }
    //convert to window format
    image = SDL_ConvertSurface(loaded, window.get_screen_surface()->format, NULL);
    if(image == NULL)
    {
        cout << "Unable to convert surface: " << SDL_GetError() << endl;
    }
    SDL_FreeSurface(loaded);
    //set up animation
    animation_frames.push_back(make_pair(0, 0));
    current_frame = 0;
}
Rect::Rect(string filename, Window &window)
{
    LoadImage(filename, window);
    sdl_rect.x = 0;
    sdl_rect.y = 0;
    sdl_rect.w = image->w;
    sdl_rect.h = image->h;
    rows = 1;
    columns = 1;
}

Rect::Rect(string filename, Window &window, int rows, int columns)
{
    LoadImage(filename, window);
    sdl_rect.x = 0;
    sdl_rect.y = 0;
    sdl_rect.w = image->w/columns;
    sdl_rect.h = image->h/rows;
    this->rows = rows;
    this->columns = columns;
}

Rect::Rect(string font_filename, string text, SDL_Color text_color)
{
    TTF_Font *font = TTF_OpenFont(font_filename.c_str(), 28);
    if(font == NULL)
    {
        cout << "Failed to load font! " << TTF_GetError() << endl;
        return;
    }
    image = TTF_RenderText_Solid(font, text.c_str(), text_color);
    if(image == NULL)
    {
        cout << "Failed to create surface! " << SDL_GetError() << endl;
        return;
    }
    sdl_rect.x = 0;
    sdl_rect.y = 0;
    sdl_rect.w = image->w;
    sdl_rect.h = image->h;
    rows = 1;
    columns = 1;
}

Rect::~Rect()
{
    SDL_FreeSurface(image);
    image = NULL;
}

void Rect::Move(int x, int y)
{
    sdl_rect.x = x;
    sdl_rect.y = y;
}

void Rect::Draw(const SDL_Rect &camera, Window &window)
{
    Draw(camera, window, animation_frames[current_frame].first, animation_frames[current_frame].second);
    //go to next frame
    current_frame = (current_frame + 1) % animation_frames.size();
}

void Rect::Draw(const SDL_Rect &camera, Window &window, int row, int column)
{
    SDL_Rect srcrect;
    //set up src rect to clip
    srcrect.x = double(column) * (image->h/rows);
    srcrect.y = double(row) * (double(image->w) / double(columns));
    srcrect.w = image->w/columns;
    srcrect.h = image->h/rows;
    //set up dstrect to draw on
    SDL_Rect dstrect = sdl_rect; dstrect.x = sdl_rect.x - camera.x;
    dstrect.y = sdl_rect.y - camera.y;
    //draw
    SDL_BlitSurface(image, &srcrect, window.get_screen_surface(), &dstrect);
}

bool Rect::CheckCollision(Rect &rect)
{
    return SDL_HasIntersection(&rect.sdl_rect, &sdl_rect);
    /*if((y + h <= rect.y) || (rect.y + rect.h <= y))
        return false;
    if((x + w <= rect.x) || (rect.x + rect.w <= x))
        return false;
    return true;*/
}

void Rect::CorrectPosition(Rect* rect)
{
    if(CheckCollision(*rect))
    {
        sdl_rect.y = rect->get_y() - sdl_rect.h;
    }
}

void Rect::CorrectPosition(vector<Rect*> rects)
{
    for(int i = 0; i < rects.size(); i++)
    {
        if(CheckCollision(*rects[i]))
        {
            //move above colliding object
            sdl_rect.y = rects[i]->get_y() - sdl_rect.h;
        }
    }
}

int Rect::get_x()
{
    return sdl_rect.x;
}

int Rect::get_y()
{
    return sdl_rect.y;
}

int Rect::get_w()
{
    return sdl_rect.w;
}

int Rect::get_h()
{
    return sdl_rect.h;
}

SDL_Rect Rect::get_rect()
{
    return sdl_rect;
}

SDL_Surface *Rect::get_image()
{
    return image;
}

void Rect::SetAnimationFrames(vector<pair<int, int> > animation_frames)
{
    this->animation_frames = animation_frames;
}
