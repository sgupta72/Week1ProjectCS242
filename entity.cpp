#include "entity.h"

using namespace std;

void Entity::HandleEvents(const SDL_Event &event)
{
}

void Entity::Update(vector<Entity*> &entities)
{
}

void Entity::Draw(SDL_Rect &camera, Window &window)
{
}
