#include <SDL2/SDL_image.h>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include "editor.h"
#include "platform.h"

using namespace std;

Editor::Editor()
{
    quit = false;
    GameLoop();
}

Editor::~Editor()
{

}

void Editor::GameLoop()
{
    const int num_types = 4;
    const int TILE_SIZE = 64;
    int width = 800;
    int height = 600;
    
    //set up window and camera
    Window window(width, height);
    SDL_Point camera_vel;
    camera_vel.x = 0;
    camera_vel.y = 0;
    SDL_Rect camera;
    camera.x = 0;
    camera.y = 0;
    camera.w = width;
    camera.h = height;
    int cur_type = 0;
    vector<pair<Rect*, int> > entities;
    //while the user has not quit
    while(!quit)
    {
        SDL_Rect mouse_rect;
        bool to_add = false;
        //handle input
        while(SDL_PollEvent(&e) != 0)
        {
            mouse_rect.x = e.motion.x;
            mouse_rect.y = e.motion.y;
            mouse_rect.w = 1;
            mouse_rect.h = 1;
            if(e.type == SDL_QUIT)
                quit = true;
            if(e.type == SDL_KEYDOWN && e.key.repeat == 0)
            {
                switch(e.key.keysym.sym)
                {
                //save the level
                case SDLK_s:
                    Save("level.txt", entities);
                    break;
                //change the type
                case SDLK_t:
                    cur_type = (cur_type + 1)%num_types;
                    break;
                //move camera
                case SDLK_UP:
                    camera_vel.y -= 10;
                    break;
                case SDLK_DOWN:
                    camera_vel.y += 10;
                    break;
                case SDLK_LEFT:
                    camera_vel.x -= 10;
                    break;
                case SDLK_RIGHT:
                    camera_vel.x += 10;
                    break;
                }
            }
            if(e.type == SDL_KEYUP && e.key.repeat == 0)
            {
                //stop moving camera
                switch(e.key.keysym.sym)
                {
                case SDLK_UP:
                    camera_vel.y += 10;
                    break;
                case SDLK_DOWN:
                    camera_vel.y -= 10;
                    break;
                case SDLK_LEFT:
                    camera_vel.x += 10;
                    break;
                case SDLK_RIGHT:
                    camera_vel.x -= 10;
                    break;
                }
            }
            if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(1))
            {
                //add entity
                to_add = true;
                for(int i = 0; i < entities.size(); i++)
                {
                    SDL_Rect entity_rect = entities[i].first->get_rect();
                    entity_rect.x -= camera.x;
                    entity_rect.y -= camera.y;
                    if(SDL_HasIntersection(&entity_rect, &mouse_rect))
                    {
                        to_add = false;
                        break;
                    }
                }
            }
            if(SDL_GetMouseState(NULL, NULL) &SDL_BUTTON(3))
            {
                //remove entity
                for(int i = 0; i < entities.size(); i++)
                {
                    SDL_Rect entity_rect = entities[i].first->get_rect();
                    entity_rect.x -= camera.x;
                    entity_rect.y -= camera.y;
                    if(SDL_HasIntersection(&entity_rect, &mouse_rect))
                    {
                        delete entities[i].first;
                        entities.erase(entities.begin() + i);
                        i--;
                    }
                }
            }
        }
        camera.x += camera_vel.x;
        camera.y += camera_vel.y;
        window.ClearWindow();
        //render entity
        if(to_add)
        {
            //platform
            if(cur_type == 0)
            {
                Rect *rect = new Rect("platform.png", window);
                rect->Move(((mouse_rect.x + camera.x) / TILE_SIZE) * TILE_SIZE,
                            ((mouse_rect.y + camera.y) / TILE_SIZE) * TILE_SIZE);
                entities.push_back(make_pair(rect, 0));            
            }
            //enemy
            else if(cur_type == 1)
            {
                Rect *rect = new Rect("enemy.png", window, 1, 11);
                rect->Move(((mouse_rect.x + camera.x) / TILE_SIZE) * TILE_SIZE,
                            ((mouse_rect.y + camera.y) / TILE_SIZE) * TILE_SIZE);
                entities.push_back(make_pair(rect, 1));
            }
            //end door
            else if(cur_type == 2)
            {
                Rect *rect = new Rect("door.png", window);
                rect->Move(((mouse_rect.x + camera.x) / TILE_SIZE) * TILE_SIZE,
                            ((mouse_rect.y + camera.y) / TILE_SIZE) * TILE_SIZE);
                entities.push_back(make_pair(rect, 2));
            }
            //bitcoin
            else if(cur_type == 3)
            {
                Rect *rect = new Rect("bitcoin.png", window);
                rect->Move(((mouse_rect.x + camera.x) / TILE_SIZE) * TILE_SIZE,
                            ((mouse_rect.y + camera.y) / TILE_SIZE) * TILE_SIZE);
                entities.push_back(make_pair(rect, 3));
            }
        }
        //draw the entities
        for(int i = 0; i < entities.size(); i++)
        {
            entities[i].first->Draw(camera, window);
        }
        window.Update();
        SDL_Delay(100);
    }
}
//save the level
void Editor::Save(string filename, vector<pair<Rect*, int> > entities)
{
    ofstream out(filename.c_str());
    for(int i = 0; i < entities.size(); i++)
    {
        out << entities[i].first->get_x() << " " << entities[i].first->get_y() << " " << entities[i].second << endl;
    }
}
