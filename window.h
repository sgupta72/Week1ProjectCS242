#ifndef WINDOW_H
#define WINDOW_H

#include <SDL2/SDL.h>
#include "rect.h"
/**
 * Class to create window to render on
**/
class Window
{
public:
    Window(int width, int height);
    ~Window();
    //get screen surface to draw on, etc.
    SDL_Surface *get_screen_surface();
    void ClearWindow();
    //update window
    void Update();
private:
    SDL_Window *window;
    SDL_Surface *screen_surface = NULL;
};
#endif
