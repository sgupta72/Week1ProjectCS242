#include <vector>
#include <string>
#include "rect.h"

using namespace std;

#ifndef MENU_H
#define MENU_H

class Menu
{
public:
    Menu(vector<string> level_names);
    ~Menu();
    //main loop
    int Loop();
private:
    bool quit;
    vector<string> level_names;
    int cur_level;
    SDL_Event e;
};

#endif
