#include "game.h"
#include "window.h"
#include "rect.h"
#include "player.h"
#include "entity.h"
#include "platform.h"
#include "enemy.h"
#include <fstream>
#include <iostream>
#include <SDL2/SDL_image.h>

using namespace std;

Game::Game(string filename)
{
    quit = false;
    died = false;
    won = false;
    collected = 0;
    GameLoop(filename);
}

void Game::GameLoop(string filename)
{
    ifstream in(filename.c_str());
    int width = 800;
    int height = 600;
    //Create window
    Window window(width, height);
    camera.x = 0;
    camera.y = 0;
    camera.w = width;
    camera.h = height;
    //Create player
    Player player(window);
    int entity_x, entity_y, type;
    vector<Enemy *> enemies;
    vector<Entity*> entities;
    vector<Rect *> bitcoins;
    Rect *exit = NULL;
    //load the file
    while((in >> entity_x >> entity_y >> type))
    {
        if(type == 0)
        {
            entities.push_back(new Platform(window, entity_x, entity_y));
        }
        else if(type == 1)
        {
            enemies.push_back(new Enemy(window, entity_x, entity_y));
        }
        else if(type == 2)
        {
            exit = new Rect("door.png", window);
            exit->Move(entity_x, entity_y);
        }
        else if(type == 3)
        {
            Rect *bitcoin = new Rect("bitcoin.png", window);
            bitcoin->Move(entity_x, entity_y);
            bitcoins.push_back(bitcoin);
        }
    }
    cout << "Number of bitcoins: " << bitcoins.size() << endl;
    //while the quit button hasn't been pressed
    while(!quit)
    {
        //handle input
        while(SDL_PollEvent(&e) != 0)
        {
            if(e.type == SDL_QUIT)
                quit = true;
            player.HandleEvents(e);
        }
        //update positions of enemies and player
        for(int i = 0; i < enemies.size(); i++)
            enemies[i]->Update(entities);
        player.Update(entities);
        //handle logic
        for(int i = 0; i < enemies.size(); i++)
        {
            int enemy_res = player.HandleEnemy(enemies[i]);
            if(enemy_res == 1)
            {
                delete enemies[i];
                enemies.erase(enemies.begin() + i);
                i--;
            }
            else if(enemy_res == 2)
            {
                died = true;
            }
        }
        //check if we have reached the exit
        SDL_Rect player_rect = player.get_rect()->get_rect();
        if(player_rect.y > 1000)
            died = true;
        SDL_Rect exit_rect = exit->get_rect();
        if(SDL_HasIntersection(&player_rect, &exit_rect))
        {
            won = true;
        }
        for(int i = 0; i < bitcoins.size(); i++)
        {
            SDL_Rect cur_bitcoin_rect = bitcoins[i]->get_rect();
            if(SDL_HasIntersection(&player_rect, &cur_bitcoin_rect))
            {
                collected++;
                bitcoins.erase(bitcoins.begin() + i);
                break;
            }
        }
        //clear window
        window.ClearWindow();
        if(!died && !won)
        {
            //draw enemies, players and other entities
            player.Draw(camera, window);
            for(int i = 0; i < enemies.size(); i++)
                enemies[i]->Draw(camera, window);
            for(int i = 0; i < entities.size(); i++)
                entities[i]->Draw(camera, window);
            exit->Draw(camera, window);
            for(int i = 0; i < bitcoins.size(); i++)
                bitcoins[i]->Draw(camera, window);
            string num_coins = "Bitoins collected: " + to_string(collected);
            SDL_Color black = {0, 0, 0};
            Rect *coins = new Rect("times.ttf", num_coins, black);
            SDL_Rect dummy_camera = {0, 0, 0, 0};
            coins->Draw(dummy_camera, window, 0, 0);
        }
        else if(died)
        {
            died = false;
            SDL_Color black = {0, 0, 0};
            Rect *you_died = new Rect("times.ttf", "You Died!", black);
            you_died->Move(400 - you_died->get_w()/2, 300 - you_died->get_h()/2);
            camera.x = 0;
            camera.y = 0;
            you_died->Draw(camera, window, 0, 0);
            //update the window
            window.Update();
            SDL_Delay(1000);
            return;
        }
        else if(won)
        {
            won = false;
            SDL_Color black = {0, 0, 0};
            Rect *you_died = new Rect("times.ttf", "You Won!", black);
            you_died->Move(400 - you_died->get_w()/2, 300 - you_died->get_h()/2);
            camera.x = 0;
            camera.y = 0;
            you_died->Draw(camera, window, 0, 0);
            //update the window
            window.Update();
            SDL_Delay(1000);
            return;
        }
        //update the window
        window.Update();
        //wait for 100 ms
        SDL_Delay(100);
    }
}

Game::~Game()
{
    SDL_Quit();
}
